const { it } = require("mocha")

describe('Basic Tests', () => {
  it('Self Sign', () => {
    // visit PrivyId login page
    cy.visit('https://app.privy.id')

    // input username
    cy.get('input[name="user[privyId]"]').type('MX3263', { force: true })
    cy.contains('CONTINUE').click()

    //input user password
    cy.get('input[name="user[secret]"]').type('9RNx2TPenAkpqGcsKi7L', { force: true })
    cy.contains('CONTINUE').click()
  });

  it('Sign and Share', () => {
    // visit PrivyId login page
    cy.visit('https://app.privy.id')

    // input username
    cy.get('input[name="user[privyId]"]').type('MX3263', { force: true })
    cy.contains('CONTINUE').click()

    //input user password
    cy.get('input[name="user[secret]"]').type('9RNx2TPenAkpqGcsKi7L', { force: true })
    cy.contains('CONTINUE').click()

    // upload document
    cy.get('button[id="v-step-0"]').click()
    cy.contains('Self Sign').click()
    cy.contains('browse').click()

    const fileName = 'Sign.pdf';

    cy.get('[type=file]').attachFile(fileName)
    cy.get('.modal-content .modal-footer button:contains("Upload")').click()
    cy.wait(2000)
    cy.contains('Continue').click()
    cy.wait(5000)
    cy.get('button[class="btn btn-danger"]').click()
    cy.contains('Done').click()
  });

  it('Change image signature', () =>{
    cy.visit('app.privy.id')
    cy.get('input[name="user[privyId]"]').type('MX3263', {
      force: true
  })
    cy.contains('CONTINUE').click()
    cy.get('input[name="user[secret]"]').type('9RNx2TPenAkpqGcsKi7L', {
      force: true
  })
    cy.contains('CONTINUE').click()
    cy.contains('Change My Signature Image').click()
    cy.contains('Add Signature').click()
    cy.get('input[id="name-initial"]').clear()
    cy.get('input[id="name-initial"]').type('Andi')
    cy.contains('Save').click()
  });
});